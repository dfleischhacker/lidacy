#!/usr/bin/env python

import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

if len(sys.argv) != 4:
    print 'Usage: <csvfile> <fieldname1> <fieldname2>' 
    sys.exit(1)

data1 = []
data2 = []
with open(sys.argv[1], 'rb') as f:
    r = csv.reader(f)
    col1 = -1
    col2 = -1
    for l in r:
        if col1 == -1:
            col1 = l.index(sys.argv[2])
            col2 = l.index(sys.argv[3])
            continue
        if l[col1].strip()  == '' or l[col2].strip() == '':
            continue
        data1.append(int(l[col1]))
        data2.append(int(l[col2]))

fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(data1, data2, s=1) #bins=(max(data)-min(data)))#, 100, normed=1, facecolor='green', alpha=0.75)

ax.set_xlabel(sys.argv[2])
ax.set_ylabel(sys.argv[3])

#ax.set_xlim(40, 160)
#ax.set_ylim(0, 0.03)
ax.grid(True)

plt.show()

#!/usr/bin/env python

import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

if len(sys.argv) != 3:
    print 'Usage: <csvfile> <fieldname>'
    sys.exit(1)

data = []
with open(sys.argv[1], 'rb') as f:
    r = csv.reader(f)
    col = -1
    for l in r:
        if col == -1:
            col = l.index(sys.argv[2])
            continue
        if l[col].strip()  == '':
            continue
        data.append(int(l[col]))

fig = plt.figure()
ax = fig.add_subplot(111)
n, bins, patches = ax.hist(data, bins=(max(data)-min(data)))#, 100, normed=1, facecolor='green', alpha=0.75)

ax.set_xlabel(sys.argv[2])
ax.set_ylabel('Number of Instances')
#ax.set_xlim(40, 160)
#ax.set_ylim(0, 0.03)
ax.grid(True)

plt.show()

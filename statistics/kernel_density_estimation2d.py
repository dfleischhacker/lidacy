#!/usr/bin/env python

import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import scipy.stats.kde as kde
import scipy.stats as stats
from pylab import plot, show, hist
import random as rand


if len(sys.argv) != 4:
    print 'Usage: <csvfile> <fieldname1> <fieldname2>'
    sys.exit(1)

data = []
with open(sys.argv[1], 'rb') as f:
    r = csv.reader(f)
    col1 = -1
    col2 = -1
    for l in r:
        if col1 == -1:
            col1 = l.index(sys.argv[2])
            col2 = l.index(sys.argv[3])
            continue
        if l[col1].strip()  == '' or l[col2].strip() == '':
            continue
        data.append((float(l[col1]), float(l[col2])))

print 'Data points: {}'.format(len(data))
sample = rand.sample(data, int(len(data)*0.1))

pdf = kde.gaussian_kde(sample)
x_flat = np.r_[data[:,0].min():rvs[:,0].max():128j]
y_flat = np.r_[rvs[:,1].min():rvs[:,1].max():128j]
x,y = np.meshgrid(x_flat,y_flat)
grid_coords = np.append(x.reshape(-1,1),y.reshape(-1,1),axis=1)

z = kde(grid_coords.T)
z = z.reshape(128,128)
x = np.linspace(min(sample), max(sample), max(sample) - min(sample))
plot(z)
#hist(sample, bins=(max(sample)-min(sample)), normed=1, alpha=.3)
# done computing distribution, check data for outliers

with open(sys.argv[1], 'rb') as f:
    r = csv.reader(f)
    col1 = -1
    col2 = -1
    for l in r:
        if col1 == -1:
            col1 = l.index(sys.argv[2])
            col2 = l.index(sys.argv[3])
            continue
        if l[col1].strip()  == '' or l[col2].strip() == '':
            continue
        prob = z.evaluate((float(l[col1]), float(l[col2])))
        print '{} has value {} which has probability {}'.format(l[0], (float(l[col1]), float(l[col2])), prob)

show()

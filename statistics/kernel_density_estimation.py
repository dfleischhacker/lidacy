#!/usr/bin/env python

import sys
import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import scipy.stats.kde as kde
import scipy.stats as stats
from pylab import plot, show, hist
import random as rand

if len(sys.argv) != 3:
    print 'Usage: <csvfile> <fieldname>'
    sys.exit(1)

data = []
index = []
with open(sys.argv[1], 'rb') as f:
    r = csv.reader(f)
    col = -1
    for l in r:
        if col == -1:
            col = l.index(sys.argv[2])
            continue
        if l[col].strip()  == '':
            continue
        data.append(float(l[col]))
        index.append((l[0], l[col]))
print 'Data points: {}'.format(len(data))
sample = data #rand.sample(data, int(len(data)*0.5))

pdf = kde.gaussian_kde(sample)
x = np.linspace(min(sample), max(sample), max(sample) - min(sample))
plot(x, pdf(x), 'r')
hist(sample, bins=(max(sample)-min(sample)), normed=1, alpha=.3)


# try to find values not too probable
results = []
prob_sum = 0
for v in index:
    probability = pdf(float(v[1]))
    #print 'Instance {} with value {} has probability of'.format(v[0], v[1], probability[0])
    results.append((v[0], v[1], probability[0]))
    prob_sum += probability
avg_prob = (prob_sum/float(len(results)))
print 'Average probability is {}'.format(avg_prob)
for i in results:
    if i[2]/avg_prob < 1.0:
        print 'Item {} has lower probability {} than average: {}, score {}'.format(i[0],i[2], i[1], i[2]/avg_prob)
show()

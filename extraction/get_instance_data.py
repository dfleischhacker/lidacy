#!/usr/bin/env python

from DBpediaUtils import Namespace
import SPARQLWrapper as s
import myarff as arff
from dateutil import parser
import pytz
import pickle
import os.path
import sys
import urllib2
import codecs
import csv
import cStringIO
import threading
import Queue
import re

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

class RowWriteWorker(threading.Thread):
    uris = Queue.Queue()

    writerLock = threading.Lock()

    def __init__(self, writer, endpoint, properties):
        threading.Thread.__init__(self)
        self.writer = writer
        self.sparql = s.SPARQLWrapper(endpoint)
        self.properties = properties

    def run(self):
        while(True):
            instance = RowWriteWorker.uris.get()
            query = generate_query_for_individual(instance, self.properties)
            #print query
            self.sparql.setQuery(query)
            self.sparql.setReturnFormat(s.JSON)
            res = self.sparql.query().convert()

            for result in res["results"]["bindings"]:
                r = generate_row_from_result(instance, self.properties, result)
                skip = True
                for e in r[1:-1]:
                    if len(e.strip()) != 0:
                        skip = False
                        break
                if skip:
                    continue
                RowWriteWorker.writerLock.acquire()
                self.writer.writerow(r)
                RowWriteWorker.writerLock.release()
            RowWriteWorker.uris.task_done()

# config #############################################################################################


# classes and their properties to retrieve from SPARQL endpoint
CLASSES = {
    'Person': ['deathDate', 'birthDate', 'deathYear', 'birthYear', 'waistSize',
        'retirementDate', 'titleDate'],
}

######################################################################################################

target_dir = './'
if len(sys.argv) == 2:
    target_dir = sys.argv[1]


ns = Namespace()

def parse_date(date):
    """
    Provides parsing for the specific date formats assigned to DBpedia
    instances, returns a tuple consisting of integer values (year, month, day)
    """
    # case 1: YYYY-MM-DD
    m = re.match(r'^(?P<year>\d\d\d\d)-(?P<month>\d\d)-(?P<day>\d\d)$', date)
    if m != None:
        return (int(m.group('year')), int(m.group('month')), int(m.group('day')))

    # case 2: --MM-DD (http://www.w3.org/2001/XMLSchema#gMonthDay)
    m = re.match(r'^--(?P<month>\d\d)-(?P<day>\d\d)', date)
    if m != None:
        return (None, int(m.group('month')), int(m.group('day')))

    print 'Not year parsed: {}'.format(date)

    # case 3: -YYYY
    m = re.match(r'^\-(?P<year>\d\d\d\d)$', date)
    if m != None:
        return (-int(m.group('year')), None, None)

    # case 4: "1955-01-01T00:00:00+02:00"
    m = re.match(r'^(?P<year>\d\d\d\d)\-01\-01T00\:00\:00\+0.\:00$', date)
    if m != None:
        return (int(m.group('year')), None, None)

    return None

def get_age(birth, death):
    """
    Approximatively computes the death age of a person born and died at the given dates
    """
    if birth[0] == None or death[0] == None:
        return None
    return (death[0] - birth[0])


def generate_query_for_individual(individual_uri, properties):
    """
    Generates a query to retrieve the given properties for all instances of the concept cls

    :type individual_uri: str
    :param individual_uri: fully qualified concept URI for individual to retrieve properties for
    :type properties: list
    :param properties: list of properties to retrieve from endpoint for the given instance

    :return: query to retrieve properties' values for given instance
    """
    query = u'SELECT * WHERE { '

    for p in properties:
        query += u' OPTIONAL {{ <{}> <{}> ?{}. }}'.format(individual_uri.encode('utf-8').replace('%0A', ''), ns.ontology(p), p.lower())

    return query + u'}'

def get_all_individuals_of_class(sparql, cls):
    """
    Returns a list of all individuals of the given class

    :type sparql SPARQLWrapper
    :param sparql instance of an initialized SPARQLWrapper
    :type cls string
    :param cls class to get individuals for
    """

    if os.path.exists('/tmp/{}.txt'.format(cls)):
        individuals = []
        with codecs.open('/tmp/{}.txt'.format(cls), encoding='utf8') as f:
            for l in f:
                individuals.append('http://{}'.format(urllib2.quote(l.encode('utf8').replace('http://', ''))))
        return individuals

    q = u"SELECT DISTINCT ?uri WHERE {{?uri a <{}>.}}".format(ns.ontology(cls))
    sparql.setQuery(q)
    sparql.setReturnFormat(s.JSON)
    res = sparql.query().convert()

    individuals = []

    with codecs.open('/tmp/{}.txt'.format(cls), 'w', encoding='utf8') as f:
        for result in res["results"]["bindings"]:
            #print result["uri"]["value"].encode('ascii')
            #print type(result["uri"]["value"])
            #u = urllib2.unquote(result["uri"]["value"].encode('ascii'))
            #print u
            u = urllib2.unquote(unicode(result["uri"]["value"]).encode('ascii')).decode('utf8')
            #print u
            individuals.append(u)
            f.write(u)
            f.write('\n')

    print 'Number of individuals: {}'.format(len(individuals))

    return individuals

def generate_row_from_result(instance, properties, res):
    """
    Uses the given SPARQL result line and generates a row for being written into the arff file
    """
    row = [instance.replace('%0A', ''),]
    for p in properties:
        p = p.lower()
        if p in res:
            if p == 'birthYear' or p == 'deathYear':
                row.append(process_sparql_result(res[p]))
            else:
                row.append(assure_numeric(process_sparql_result(res[p])))
        else:
            row.append(u'')

    # compute age based on birthDate and deathDate
    if 'birthdate' in res:
        bdate = parse_date(res['birthdate']['value'])
        if bdate != None:
            if bdate[0] == None:
                row.append(u'')
            else:
                row.append(unicode(bdate[0]))
            row.append(unicode(bdate[1]))
            row.append(unicode(bdate[2]))
        else:
            row.append(u'')
            row.append(u'')
            row.append(u'')
    else:
        row.append(u'')
        row.append(u'')
        row.append(u'')

    if 'deathdate' in res:
        ddate = parse_date(res['deathdate']['value'])
        if ddate != None:
            if ddate[0] == None:
                row.append(u'')
            else:
                row.append(unicode(ddate[0]))
            row.append(unicode(ddate[1]))
            row.append(unicode(ddate[2]))
        else:
            row.append(u'')
            row.append(u'')
            row.append(u'')
    else:
        row.append(u'')
        row.append(u'')
        row.append(u'')


    if 'birthdate' in res and 'deathdate' in res:
        ddate = parse_date(res['deathdate']['value'])
        bdate = parse_date(res['birthdate']['value'])
        if ddate != None and bdate != None:
            diff = get_age(bdate, ddate)
            if diff == None:
                row.append(u'')
            else:
                row.append(unicode(diff))
        else:
            row.append(u'')
    else:
        row.append(u'')

    # compute age based on birthYear and deathYear
    if 'birthyear' in res and 'deathyear' in res:
        ddate = parse_date(res['deathyear']['value'])
        bdate = parse_date(res['birthyear']['value'])
        if ddate != None and bdate != None:
            diff = get_age(bdate, ddate)
            if diff == None:
                row.append(u'')
            else:
                row.append(unicode(diff))
        else:
            row.append(u'')
    else:
        row.append(u'')

    if row[-1] != '' and row[-2] != '':
        if int(row[-1]) != int(row[-2]) and int(row[-1]) != (int(row[-2])+1):
            row.append(u'n')
        elif int(row[-1]) < 0 or int(row[-2]) < 0:
            row.append(u'n')
        else:
            #assume as correct
            row.append(u'y')
    else:
        if row[-1] != '' and int(row[-1]) < 0:
            row.append(u'n')
        elif row[-2] != '' and int(row[-2]) < 0:
            row.append(u'n')
        else:
            #assume as correct
            row.append(u'y')

    return row

def assure_numeric(val):
    """
    Assures that the given value is an integer value resp a string convertible into such a value.
    If not an empty string is returned
    """
    if re.match('^-?[0-9]*$', val) != None:
        return val
    else:
        return u''

def is_numeric(val):
    """
    Checks if the given value is an integer value resp a string convertible into such a value.
    """
    return (re.match('^[0-9]*$', val) != None)

def process_sparql_result(res_dict):
    """
    Processes the given dictionary trying to detect the datatype and converts it to a common format for this datatype.
    The result of this process is written back into the value field of the given 
    """
    if not res_dict['type'] == 'typed-literal':
        return res_dict['value'].decode('utf8')

    if res_dict['datatype'] == 'http://www.w3.org/2001/XMLSchema#date':
        d = parse_date(res_dict['value'])
        if d == None:
            return u''
        if d[0] == None:
            return u'0000{:02}{:02}'.format(d[1],d[2])
        else:
            return u'{:04}{:02}{:02}'.format(d[0],d[1],d[2])

    if res_dict['datatype'] == 'http://www.w3.org/2001/XMLSchema#gYear':
        d = parse_date(res_dict['value'])
        if d == None or d[0] == None:
            return u''
        return unicode(d[0])

    if res_dict['datatype'] == 'http://www.w3.org/2001/XMLSchema#gYearMonth':
        d = parse_date(res_dict['value'])
        if d == None:
            return u''
        if d[0] == None:
            return u'0000{:02}{:02}'.format(d[1],d[2])
        else:
            return u'{:04}{:02}{:02}'.format(d[0],d[1],d[2])

    if res_dict['datatype'] == 'http://www.w3.org/2001/XMLSchema#integer':
        return res_dict['value']

    return res_dict['value'].decode('utf8')

def write_individual_to_arff(sparql, arff_writer, instance, properties):
    """
    Generates the row for the given individual which later on is added into the file
    """
    query = generate_query_for_individual(instance, properties)
    sparql.setQuery(query)
    sparql.setReturnFormat(s.JSON)
    res = sparql.query().convert()

    for result in res["results"]["bindings"]:
        r = generate_row_from_result(instance, properties, result)
        empty = True
        for v in r[1:]:
            if v != '':
                empty = False
                break
        if not empty:
            arff_writer.writerow(r)


if __name__ == '__main__':
    sw = s.SPARQLWrapper("http://ole.informatik.uni-mannheim.de:8890/sparql")

    for key in CLASSES:
        print("Generating transaction table for concept {}".format(key))
        headers = ['uri']
        headers.extend(CLASSES[key])
        headers.append('byear')
        headers.append('bmonth')
        headers.append('bday')
        headers.append('dyear')
        headers.append('dmonth')
        headers.append('dday')
        headers.append('ageDates')
        headers.append('ageYears')
        headers.append('correct')
        individuals = get_all_individuals_of_class(sw, key)
        arff_writer = UnicodeWriter(open('{}{}.csv'.format(target_dir, key), 'wb'))
        arff_writer.writerow(headers)

        threads = [RowWriteWorker(arff_writer, "http://ole.informatik.uni-mannheim.de:8890/sparql", CLASSES[key]) for i in range(12)]

        for t in threads:
            t.setDaemon(True)
            t.start()

        for i in individuals:
            RowWriteWorker.uris.put(i)

        RowWriteWorker.uris.join()

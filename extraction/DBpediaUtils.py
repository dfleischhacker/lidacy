class Namespace:
    def ontology(self, concept):
        """
        Adds the DBpedia ontology URL prefix to the given local names
        """
        return 'http://dbpedia.org/ontology/{}'.format(concept)

    def resource(self, concept):
        """
        Adds the DBpedia resource URL prefix to the given local names
        """
        return 'http://dbpedia.org/resource/{}'.format(concept)

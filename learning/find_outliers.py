#!/usr/bin/env python

# Tries to detect erroneous values by using the notion of outliers

import sys
import csv
import pandas as pd
import statsmodels.nonparametric.kde as kde

class ErrorDetector:
    def __init__(self, csv):
        """
        Initializes the detector to work on the given csv file
        """
        #with open(csv, 'rb') as f:
        #    firstLine = True
        #    reader = csv.reader(f)
        #    for l in reader:
        #        if firstLine:
        #            firstLine = False
        #            self.headers = [i for i in l]
        #        curRow = {}
        #        for i in xrange(len(l)):
        #            curRow[self.headers[i]] = l[i]
        #        self.data.append(curRow)
        self.data = pd.read_csv(open(csv, 'rb'))
        self.cur_density = None
 
    def detect_errors_by_density_estimation(self, column_name, kernel_type='gau'):
        """
        Tries to detect errors using a density function of the given type for the given column name.

        Possible kernel types are:
            biw for biweight
            cos for cosine
            epa for Epanechnikov
            gau for Gaussian.
            tri for triangular
            triw for triweight
            uni for uniform
        """
        col = self.data[column_name]
        print 'Detecting errors on column {}'.format(column_name)
        nonnull = col.dropna()
        print 'Number of non-null values: {}'.format(len(nonnull))
        print 'Min: {}, Max: {}'.format(nonnull.min(), nonnull.max())
        print 'Mean value: {}'.format(nonnull.mean())
        print 'Median value: {}'.format(nonnull.median())
        print 'Standard deviation: {}'.format(nonnull.std())
        print 'Variance: {}'.format(nonnull.var())
        
        dens = kde.KDE(nonnull)

        if kernel_type !='gau':
            dens.fit(kernel_type, fft=False, gridsize=50)
        else:
            dens.fit(kernel_type)
            
        self.plot_kde(nonnull, dens, 'plot_kde_{}'.format(column_name))

        probabilities = []
        probability_sum = 0.0

        id_index = self.data.columns.tolist().index('uri')
        val_index = self.data.columns.tolist().index(column_name)

        count = 0
        todo = len(self.data[column_name].dropna())
        for k, v in self.data[column_name].dropna().iteritems():
            sys.stdout.write("\r%###.2f%%" % (count/float(todo)))
            count += 1
            p = self.dens_evaluate(dens, v)
            uri = self.data.ix[k]['uri']
            probabilities.append((k, v, p))
            probability_sum += p

        average_p = probability_sum/float(len(probabilities))

        outliers = []
        for p in probabilities:
            outliers.append((p[0], p[1], p[2][0], p[2][0]/average_p))
        self.write_to_csv('outliers_{}.csv'.format(column_name), outliers)


    def dens_evaluate(self, dens, val):
        """
        Evaluates the given density at the specified point. Caches the result
        """
        if self.cur_density == None or self.cur_density != dens:
            self.cur_density = dens
            self.density_cache = {}
        if val not in density_cache:
            density_cache[val] = dens.evaluate(val)
        return density_cache[val]

    def plot_kde(self, data, dens, filename):
        import numpy as np
        from pylab import plot, show, hist, savefig

        x = np.linspace(min(data), max(data), (max(data) - min(data))/int(20))
        plot(x, dens.evaluate(x), 'r')
        hist(data, bins=(max(data)-min(data)), normed=1, alpha=.3)
        
        savefig(filename)

    def write_to_csv(self, filename, data):
        import csv
        with open(filename, 'w') as f:
            w = csv.writer(f)
            for l in data:
                w.writerow(l)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: <csvfile> <columnname>'
        sys.exit()

    detector = ErrorDetector(sys.argv[1])
    detector.detect_errors_by_density_estimation(sys.argv[2], 'gau')



import csv
from sklearn import tree
from StringIO import StringIO

# load data from csv file
samples = []
labels = []

with open('/home/daniel/Person_annotated.csv', 'rb') as f:
  reader = csv.reader(f)
  firstLine = True
  for l in reader:
    if firstLine:
      firstLine = False
      continue
    if l[9] == '':
      continue
    label = l[10]
    if int(l[9]) > 150:
      label = 'n'
    samples.append([l[9],])
    if label == 'y':
      labels.append(1)
    else:
      labels.append(-1)

clf = tree.DecisionTreeClassifier()#max_depth=4)
clf = clf.fit(samples, labels)
out = StringIO()
out = tree.export_graphviz(clf, out_file=out)
print out.getvalue()
